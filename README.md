# Networks

# **SmartBCH**
- SDK: https://www.npmjs.com/package/@koingfu.com/sdk-bch
- network id: 10000
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0xc4eb62f900ae917f8F86366B4C1727eb526D1275
- RPC address: [https://smartbch.greyh.at](https://smartbch.greyh.at/)
- Multicall address: [https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC](https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC)
- Farm address:

# **SmartBCH Amber Testnet**

- network id: 10001 (or hex 0x2711)
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0x21c7FD76B440e443aafb364f31396a3Ea0b3Abe0
- RPC address: [https://moeing.tech:9545/](https://moeing.tech:9545/)
- Multicall address: [https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a](https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a)
- Farm address:

# **Ethereum Polygon**

- network id: 137
- Core/Factory address: 0x83B9b8B982fcb0f09d2f009ad12182d4d945F1E3
- Router/Periphery address: 0x6945b33dAA3fbD747e04D8859B11787dB463127c
- wETH address: 0x7ceb23fd6bc0add59e62ac25578270cff1b9f619
- RPC address: [https://rpc-mainnet.matic.network](https://rpc-mainnet.matic.network/)
- Multicall address: [https://polygonscan.com/address/0xa1B2b503959aedD81512C37e9dce48164ec6a94d/transactions#code](https://polygonscan.com/address/0xa1B2b503959aedD81512C37e9dce48164ec6a94d/transactions#code)
- Farm address:

# **Ethereum Polygon Testnet**

- network id: 80001
- Core/Factory address: not yet deployed
- Router/Periphery address: not yet deployed
- wETH address: 0x6a17dc2aeedd09d5c1b3e7e963eca0697296d019
- RPC address: [https://rpc-mumbai.matic.today](https://rpc-mumbai.matic.today/)
- Multical2:
- Farm address:

# **Avax**

- network id: 43114
- Core/Factory address: 0x34362A81cdCe41b726B92AA1D6654205abB8f1B0
- Router/Periphery address: 0x81f85FC68d6A1502a2AF542d23796E643Af661BC
- wAVA address: 0xb31f66aa3c1e785363f0875a1b74e27b85fd66c7
- RPC address: [https://api.avax.network/ext/bc/C/rpc](https://api.avax.network/ext/bc/C/rpc)
- Multicall address: [https://snowtrace.io/find-similar-contracts?a=0xa00FB557AA68d2e98A830642DBbFA534E8512E5f&lvl=5](https://snowtrace.io/find-similar-contracts?a=0xa00FB557AA68d2e98A830642DBbFA534E8512E5f&lvl=5)
- Farm address:

# **Avax Testnet**

- network id: 43113
- Core/Factory address: 0x34362A81cdCe41b726B92AA1D6654205abB8f1B0
- Router/Periphery address: 0x81f85FC68d6A1502a2AF542d23796E643Af661BC
- wAVA address: 0xb31f66aa3c1e785363f0875a1b74e27b85fd66c7
- RPC address: [https://api.avax.network/ext/bc/C/rpc](https://api.avax.network/ext/bc/C/rpc)
- Multicall address: [https://snowtrace.io/find-similar-contracts?a=0xa00FB557AA68d2e98A830642DBbFA534E8512E5f&lvl=5](https://snowtrace.io/find-similar-contracts?a=0xa00FB557AA68d2e98A830642DBbFA534E8512E5f&lvl=5)
- Farm address:


# **Binance Smart Chain**

- network id: 56
- Core/Factory address: 0x5873D455909cA064FBF3290DC34bf9fb46C16cdC
- Router/Periphery address: **0xde49191E1E28D30e2cb1bd3696054B91e4A9ef05**
- wBNB address: 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c
- RPC address: [https://bsc-dataseed.binance.org/](https://bsc-dataseed.binance.org/)
- Multicall addres: [https://bscscan.com/address/0x1Ee38d535d541c55C9dae27B12edf090C608E6Fb#code](https://bscscan.com/address/0x1Ee38d535d541c55C9dae27B12edf090C608E6Fb#code)
- Explorer: [https://www.smartscan.cash/](https://www.smartscan.cash/)
- Farm address:

# **Binance Smart Chain Testnet**

- network id: 56
- Core/Factory address: 0x5873D455909cA064FBF3290DC34bf9fb46C16cdC
- Router/Periphery address: **0xde49191E1E28D30e2cb1bd3696054B91e4A9ef05**
- wBNB address: 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c
- RPC address: [https://bsc-dataseed.binance.org/](https://bsc-dataseed.binance.org/)
- Multicall addres: [https://bscscan.com/address/0x1Ee38d535d541c55C9dae27B12edf090C608E6Fb#code](https://bscscan.com/address/0x1Ee38d535d541c55C9dae27B12edf090C608E6Fb#code)
- Explorer: [https://www.smartscan.cash/](https://www.smartscan.cash/)
- Farm address:


